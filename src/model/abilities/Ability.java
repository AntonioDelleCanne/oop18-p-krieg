package model.abilities;

/**
 * Models an exclusive skill for a Unit.
 */
public interface Ability {

    /**
     * @return the unique identifier of this ability.
     */
    String getId();
}
