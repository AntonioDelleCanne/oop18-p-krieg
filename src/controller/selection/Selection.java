package controller.selection;

/**
 * describes the selection in a case of the game map.
 */
public interface Selection {
    /**
     * 
     * @return the id of the selection
     */
    String getId();
}
