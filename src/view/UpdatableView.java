package view;

import controller.Updater;

/**
 * a marker interface.
 *
 */
public interface UpdatableView extends Updater {

}
